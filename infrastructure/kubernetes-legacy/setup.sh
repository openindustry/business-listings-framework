#!/bin/sh

for f in pvc/*.yaml secrets/*.yaml deployments/*.yaml services/*.yaml
do
    kubectl create -f $f --validate=false
done
